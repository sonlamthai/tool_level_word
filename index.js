const _ = require('lodash')
// const wordList = require('./wordList')
const fs = require('fs');
const {
    random,
    compact,
    size,
    max
} = require('lodash');
const {
    config
} = require('process');
var text = fs.readFileSync("./assets/wordofwonder.txt", "utf-8");
// var text = fs.readFileSync("./WordNormal_E.txt", "utf-8");
// console.log(text);
// var text2 = fs.readFileSync("./assets/words.txt", "utf-8");
var text2 = fs.readFileSync("./WordHard_E.txt", "utf-8");
var textByLine = text.replace(/;/g, "").split(/\r\n|\n|\r/);
// var textByLine = text.replace(/;/g, "").split(" ");
var textByLineHard = text2.replace(/;/g, "").split(/\r\n|\n|\r/);
let Board = require('./BoardGird')



var filterWordList = function (pattern, minLength) {
    var filteredWords = _.filter(textByLine, function (word) {
        return word.length <= pattern.length &&
            word.length >= minLength;
    });
    filteredWords = _.filter(filteredWords, function (word) {
        var str1 = pattern;
        var str2 = word;

        s1 = str1.split('');

        s2 = str2.split('');


        var i = s1.length + 1;
        while (i--) {
            if (s2.indexOf(s1[i]) >= 0)
                s2.splice(s2.indexOf(s1[i]), 1);
        }
        return s2.length === 0;
    });

    return filteredWords;
};
var getRandomWordWithLength = function (length, minLength) {
    var filteredWords = _.filter(textByLine, function (word) {
        return word.length <= length && word.length >= minLength;
    });
    // console.log(filteredWords);
    var n = Math.floor(Math.random() * filteredWords.length)
    let item = filteredWords[n]
    // console.log(item);
    return item;
}
var filterWordListHard = function (pattern, minLength) {
    var filteredWords = _.filter(textByLineHard, function (word) {
        return word.length <= pattern.length && word.length >= minLength;
    });
    filteredWords = _.filter(filteredWords, function (word) {
        var str1 = pattern;
        var str2 = word;

        s1 = str1.split('');

        s2 = str2.split('');


        var i = s1.length + 1;
        while (i--) {
            if (s2.indexOf(s1[i]) >= 0)
                s2.splice(s2.indexOf(s1[i]), 1);
        }
        return s2.length === 0;
    });
    return filteredWords;
};
var randomPosition = function (col, row, arrwWords,num,maxsize) {

    arrwWords.sort((a, b) => {
        return b.length - a.length;
    })
    let wordArrayCt  = [];
    for (let i = 0 ;i< arrwWords.length ;i++ ) {
        let txt = arrwWords[i];
        if(!txt) continue;
        let word = {
            word:txt,
            displayWord:txt,
            clue:null,
        }
        wordArrayCt[i] = word;
        
    }
   let depth = wordArrayCt.length;
   let bestSocre = {
       score:0
   }
   let bestboard 
   let condition = false
  
   do {
        // board.restGrid()
        let board = new Board(row,row,num,wordArrayCt);
        let xP = getRandomInt(Math.ceil(row - 1), Math.floor(0));
        let yP = getRandomInt(Math.ceil(col - 1), Math.floor(0));
        let randDir = getRandomInt(1, 0)
        let direction0 = randDir == 0 ? true :false;
        n =  Math.floor(Math.random() * wordArrayCt.length)
        board.makePutWord(wordArrayCt[n],xP,yP,false);
        let check = board.generateBoard();
        if(check== true){
            condition = true;
            bestboard = board;
            break;
        }
   } while (!condition);
   
    // for (let i = 0; i < col; i++) {
    //     for (let j = 0; j < row; j++) {
    //        board.makePutWord(wordArrayCt[0],i,j,false);
    //        let scoreItem = board.minimax(depth-1);
    //        if(scoreItem.score>bestSocre.score){
    //             bestSocre = scoreItem;
    //        }
    //        board.undo();
    //     }
        
    // }
    
    let  obj =  bestboard.getGird();
    let bgrid = createGird(row,row,obj.b,obj.activeWordList);
    console.log("GRID", bgrid.grid);
    // console.log(obj);
    let mainobj = {}
    for (let index = 0; index < obj.activeWordList.length; index++) {
       
        const element =  obj.activeWordList[index];
        let direction = element.vertical == true ? "W":"H";
        let wordObj2 = {}
        putObj(wordObj2, element.y, element.x, direction);
        mainobj[element.word] = wordObj2
        // backUpWithArrWord(element.word,element.clue,element.x,element.y,element.vertical,bgrid.grid,bgrid.griddebug,col,row)
    }
    return {
        words: mainobj,
        gird: bgrid.grid
    };
    
}
var createGird = function(cols,rows,girddebug,activeWordList){
    var grid = new Array(cols);
    // var girddebug = new Array(cols);
    for (var i = 0; i < cols; i++) {
            grid[i] = new Array(rows);
            // girddebug[i] = new Array(rows)
        for (var j = 0; j < rows; j++) {
            grid[i][j] = 0;
            if(girddebug[i][j]!=" "){
                grid[i][j] = 1;
            }
           
            // girddebug[i][j] = " ";
        }
    } 
    let check = false
    for (let index = 0; index < rows; index++) {
        const element = grid[cols-1][index];
        if(element==1){
            check = true;
            break;
        }
    }  
    if(check==false){
        grid.splice(cols-1,1);
    }
    // let check2 = false
    // for (let index = 0; index < rows; index++) {
    //     const element = grid[0][index];
    //     if(element==1){
    //         check2 = true;
    //         break;
    //     }
    // }  
    // if(check2==false){
    //     grid.splice(0,1);
    //     for (let index = 0; index < activeWordList.length; index++) {
    //         const element = activeWordList[index];
    //         element.x = element.x-1; 
    //     }
    // }
    let check3 = false;
    for (let index = 0; index < grid.length; index++) {
        const element = grid[index][rows-1];
        if(element==1){
            check3 = true;
            break;
        }
    }  
    if(check3==false){
        for (let index = 0; index <  grid.length; index++)
        grid[index].splice(rows-1,1);
    }
    // let check4 = false;
    // for (let index = 0; index < grid.length; index++) {
    //     const element = grid[index][0];
    //     if(element==1){
    //         check4 = true;
    //         break;
    //     }
    // }  
    // if(check4==false){
    //     for (let index = 0; index <  grid.length; index++)
    //     grid[index].splice(0,1);
       
    // }
    var r = {grid:grid ,griddebug:girddebug};
    return r; 
}
cutGrid = function(grid){
    for (var i = 0; i < grid.length; i++){
        for (var j = 0; j < grid[i].length; j++){

        }
    }
}
 var backUpWithArrWord = function(word, clue, x, y, vertical,grid,girddebug,GRID_HEIGHT,GRID_WIDTH) {
     
    if (vertical) {
        if (word.length + x < GRID_HEIGHT) {
            for (i = 0; i < word.length; i++) {
                grid[x + i][y] = 1;
                girddebug[x + i][y] = word[i];
            }
            // wordPlaced = true;
        }
    } else {
        if (word.length + y < GRID_WIDTH) {
            for (i = 0; i < word.length; i++) {
                grid[x][y + i] = 1;
                girddebug[x][y + i] = word[i];
            }
            // wordPlaced = true;
        }
    }
    return {grid:grid ,griddebug:girddebug}
}

var fitboard = function(arrwWords,arrWordObj,arr,col,row){
    
}
let scores = {
    X: 10,
    O: 20,
    tie: 0
};

var putObj = function (obj, x, y, direction) {
    obj['direction'] = direction;
    obj['pos'] = [y, x];
    obj['inGrid'] = true;
}


var getArrWords = function (numNomal, numHard, text, minLength) {
    let arrHard = [];
    let arrNomal = [];
    let enough = [];
    let nomal = filterWordList(text, minLength);
    let hard = filterWordListHard(text, minLength);
    let haveHard = false;
    if (!nomal) {
        nomal = []
    }
    if (!hard) {
        hard = [];
    }
    if (nomal.length < numNomal && hard.length < numHard) return {
        arrInGrid: [],
        arrNotInGrid: []
    };;
    if (nomal.length + hard.length < numNomal + numHard) return {
        arrInGrid: [],
        arrNotInGrid: []
    };

    if (nomal.length < numNomal || hard.length < numHard) {
        if (hard.length < numHard) {
            arrHard = hard;
            let numRH = numHard - hard.length;
            let numRMomal = nomal.length - 1 + numRH;
            for (let iN = 0; iN < numRMomal; iN++) {
                var n = Math.floor(Math.random() * nomal.length)
                var item = nomal[n];
                nomal.splice(n, 1);
                arrNomal.push(item);
            }
            return {
                arrInGrid: arrNomal.concat(arrHard),
                arrNotInGrid: nomal.concat(hard)
            };
        }
        if (nomal.length < numNomal) {
            arrNomal = nomal;
            let numRN = numNomal - nomal.length;
            let numRMHard = hard.length + numRN;
            for (let iH = 0; iH < numRMHard; iH++) {
                var n = Math.floor(Math.random() * hard.length)
                var item = hard[n];
                hard.splice(n, 1);
                arrHard.push(item);
            }
            return {
                arrInGrid: arrNomal.concat(arrHard),
                arrNotInGrid: nomal.concat(hard)
            };
        }

    } else {

        for (let iN = 0; iN < numNomal; iN++) {

            if (nomal.indexOf(text) >= 0) {
                nomal.splice(nomal.indexOf(text), 1);
                arrNomal.push(text);
                continue;
            }
            var n = Math.floor(Math.random() * nomal.length)
            var item = nomal[n];
            nomal.splice(n, 1);
            arrNomal.push(item);
        }
        for (let iH = 0; iH < numHard; iH++) {
            if (hard.indexOf(text) >= 0) {
                hard.splice(hard.indexOf(text), 1);
                arrHard.push(text);
                continue;
            }
            var n = Math.floor(Math.random() * hard.length)
            var item = hard[n];
            hard.splice(n, 1);
            arrHard.push(item);
        }
        return {
            arrInGrid: arrNomal.concat(arrHard),
            arrNotInGrid: nomal.concat(hard)
        };
    }
    return {
        arrInGrid: [],
        arrNotInGrid: []
    };


}
var getRandomInt = function (max, min = 0) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var getArrWordsCF = function (element) {
    let condition = false;
    let arrayWord = [];
    let letters;
    let length = getMaxLength(element.arrword);
    let maxsize = getMaxSize(element.size);
    let arrNotInGrid;
    let num = getNum(element.arrword)
    do {

        arrayWord = [];
        let word = getRandomWordWithLength(length, 3);
        let tcf = getArrWords(num, 0, word, 3)
        // console.log(tcf);
        let arrWordObj = tcf.arrInGrid;
        arrNotInGrid = tcf.arrNotInGrid;
        if (arrWordObj.length == 0) continue;
        letters = word

        for (let index = 0; index < element.arrword.length; index++) {
            let wordcf = element.arrword[index];
            if (!wordcf) {
                console.log(wordcf, 'sai');

            }
            if(!arrWordObj) continue
            let wordMax = _.filter(arrWordObj, function (word) {
                return word.length == wordcf.length;
            });
            if (wordMax.length < wordcf.numWord) {
                condition = false;
                break
            }
            condition = true;
            let word = wordMax.pop();

            condition = true;
            if (word)
                arrayWord.push(word);
            for (let i = 0; i < wordcf.numWord - 1; i++) {
                var n = Math.floor(Math.random() * wordMax.length);
                let word = wordMax[n];
                arrayWord.push(word);
                wordMax.splice(n, 1);
            }


        }
    } while (!condition);

    return {
        arrayWord: arrayWord,
        wordletter: letters,
        size: maxsize,
        arrNotInGrid: arrNotInGrid,
        num:num
    }
}
var getMaxSize = function (size) {
    if (size[0] > size[1]) {
        return size[0]
    } else {
        return size[1]
    }
}
var getNum = function (arrword) {
    let num = 0
    for (let index = 0; index < arrword.length; index++) {
        const element = arrword[index];
        num += element.numWord

    }
    return num;
}
var getMaxLength = function (arrword) {
    let max = 0
    for (let index = 0; index < arrword.length; index++) {
        const element = arrword[index];
        if (element.length >= max) {
            max = element.length;
        }

    }
    return max;
}
makeLevelConfig = function () {
    let levels = [];
    let raw = fs.readFileSync("./config.json");
    let configLevel = JSON.parse(raw)
    for (let i = 0; i < configLevel.length; i++) {
        const element = configLevel[i];
        let bc = getArrWordsCF(element)
        console.log(bc);
        let arrWordObj = bc.arrayWord;
        let word = bc.wordletter;
        let size = bc.size
        if (arrWordObj)
            if (arrWordObj.length > 0) {
                let level = {}
                let letters = word.split("");
                let obj = randomPosition(element.size[0]+1, element.size[1]+1, arrWordObj,bc.num,size);
                if (!obj) continue;
                level.letters = letters;
                level.gird = obj.gird;
                level.words = obj.words;
                levels.push(level);
                for (let index = 0; index < bc.arrNotInGrid.length; index++) {
                    const txt = bc.arrNotInGrid[index];
                    level.words[txt] = {
                        inGrid: false
                    }
                }
            }
    }
    fs.writeFile('result.json', JSON.stringify(levels), (err) => {
        if (err) throw err;
        console.log('ĐÃ CHẠY XONG');
    });
}
var duplicates = function (arrTF, level) {
    let arr = [];
    arr = arr.concat(arrTF);
    if (arr.length == 0) return false
    for (let im = 0; im < level.length; im++) {
        const element = level[im];
        if (arr.length == element.length) {
            var i = arr.length + 1;
            while (i--) {
                if (arr.indexOf(element[i]) >= 0)
                    arr.splice(arr.indexOf(element[i]), 1);
            }
            if (arr.length == 0) {
                return true;
            }
        }
    }
    return false;
}
  function checkFitScore(word, x, y, vertical) {
        var fitScore = 1; //default is 1, 2+ has crosses, 0 is invalid due to collision

        if (vertical) { //vertical checking
            for (i = 0; i < word.length; i++) {
                if (i == 0 && x > 0) { //check for empty space preceeding first character of word if not on edge
                    if (grid[x - 1][y].targetChar != EMPTYCHAR) { //adjacent letter collision
                        fitScore = 0;
                        break;
                    }
                } else if (i == word.length && x <GRID_HEIGHT) { //check for empty space after last character of word if not on edge
                    if (grid[x + i + 1][y].targetChar != EMPTYCHAR) { //adjacent letter collision
                        fitScore = 0;
                        break;
                    }
                }
                if (x + i <GRID_HEIGHT) {
                    if (grid[x + i][y].targetChar == word[i]) { //letter match - aka cross point
                        fitScore += 1;
                    } else if (grid[x + i][y].targetChar != EMPTYCHAR) { //letter doesn't match and it isn't empty so there is a collision
                        fitScore = 0;
                        break;
                    } else { //verify that there aren't letters on either side of placement if it isn't a crosspoint
                        if (y <GRID_WIDTH - 1) { //check right side if it isn't on the edge
                            if (grid[x + i][y + 1].targetChar != EMPTYCHAR) { //adjacent letter collision
                                fitScore = 0;
                                break;
                            }
                        }
                        if (y > 0) { //check left side if it isn't on the edge
                            if (grid[x + i][y - 1].targetChar != EMPTYCHAR) { //adjacent letter collision
                                fitScore = 0;
                                break;
                            }
                        }
                    }
                }

            }

        } else { //horizontal checking
            for (i = 0; i < word.length; i++) {
                if (i == 0 && y > 0) { //check for empty space preceeding first character of word if not on edge
                    if (grid[x][y - 1].targetChar != EMPTYCHAR) { //adjacent letter collision
                        fitScore = 0;
                        break;
                    }
                } else if (i == word.length - 1 && y + i <GRID_WIDTH - 1) { //check for empty space after last character of word if not on edge
                    if (grid[x][y + i + 1].targetChar != EMPTYCHAR) { //adjacent letter collision
                        fitScore = 0;
                        break;
                    }
                }
                if (y + i <GRID_WIDTH) {
                    if (grid[x][y + i].targetChar == word[i]) { //letter match - aka cross point
                        fitScore += 1;
                    } else if (grid[x][y + i].targetChar != EMPTYCHAR) { //letter doesn't match and it isn't empty so there is a collision
                        fitScore = 0;
                        break;
                    } else { //verify that there aren't letters on either side of placement if it isn't a crosspoint
                        if (x <GRID_HEIGHT) { //check top side if it isn't on the edge
                            if (grid[x + 1][y + i].targetChar != EMPTYCHAR) { //adjacent letter collision
                                fitScore = 0;
                                break;
                            }
                        }
                        if (x > 0) { //check bottom side if it isn't on the edge
                            if (grid[x - 1][y + i].targetChar != EMPTYCHAR) { //adjacent letter collision
                                fitScore = 0;
                                break;
                            }
                        }
                    }
                }

            }
        }

        return fitScore;
    }


// makeLevel();
makeLevelConfig();